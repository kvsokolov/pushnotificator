﻿namespace PushNotificator.PushNotificator
{
    public interface IPushNotificator
    {
        void PushNotification(string notification);
    }
}
