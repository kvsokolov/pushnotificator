﻿using Microsoft.Extensions.Logging;

namespace PushNotificator.PushNotificator
{
    public class LoggingNotificator:IPushNotificator
    {
        private readonly ILogger<LoggingNotificator> _logger;

        public LoggingNotificator(ILogger<LoggingNotificator> logger)
        {
            _logger = logger;
        }

        public void PushNotification(string notification)
        {
            _logger.LogDebug($"{nameof(LoggingNotificator)} push notification: {notification}");
        }
    }
}
