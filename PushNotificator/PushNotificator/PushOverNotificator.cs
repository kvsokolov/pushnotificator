﻿using System.Collections.Specialized;
using System.Net;
using Microsoft.Extensions.Configuration;

namespace PushNotificator.PushNotificator
{
    public class PushOverNotificator : IPushNotificator
    {
        private static string _requestedUrl;
        private static string _appToken;
        private static string _userKey;
        public PushOverNotificator(IConfiguration configuration)
        {
            _appToken = configuration["PushOverSettings:AppToken"];
            _userKey = configuration["PushOverSettings:UserKey"];
            _requestedUrl = configuration["PushOverSettings:RequestUrl"];
        }

        public void PushNotification(string notification)
        {
            var parameters = new NameValueCollection
            {
                {"token", _appToken},
                {"user", _userKey},
                {"message", notification}
            };
            using (var client = new WebClient())
            {
                client.UploadValues(_requestedUrl, parameters);
            }
        }
    }
}
