﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PushNotificator.PushNotificator;

namespace PushNotificator.Controllers
{
    [Route("api/[controller]")]
    public class PushNotificationController : Controller
    {
        private readonly ILogger _logger;
        private readonly IPushNotificator _notificator;

        public PushNotificationController(ILogger<PushNotificationController> logger, IPushNotificator notificator)
        {
            _logger = logger;
            _notificator = notificator;
        }

        [HttpPost]
        public IActionResult Post(string notification)
        {
            _logger.LogDebug($"Push notification message:{notification}");

            if (string.IsNullOrWhiteSpace(notification))
                return BadRequest("Notification can't be empty");

            try
            {
                _logger.LogDebug($"Push notification message done:{notification}");
                _notificator.PushNotification(notification);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error while push notification:{ex.Message} {ex.StackTrace}");
                return StatusCode(500, ex.Message);
            }
        }
    }
}
