using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using PushNotificator.Controllers;
using PushNotificator.PushNotificator;

namespace PushNotificator.Tests
{
    public class PushNotificationControllerTest
    {
        [Test]
        public void SuccessLoggingNotificatorTest()
        {
            var logger1 = new Mock<ILogger<PushNotificationController>>();
            var logger2 = new Mock<ILogger<LoggingNotificator>>();

            var controller = new PushNotificationController(logger1.Object, new LoggingNotificator(logger2.Object));
            var result = controller.Post("test");
            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public void EmptyNotification()
        {
            var logger1 = new Mock<ILogger<PushNotificationController>>();
            var logger2 = new Mock<ILogger<LoggingNotificator>>();

            var controller = new PushNotificationController(logger1.Object, new LoggingNotificator(logger2.Object));
            var result = controller.Post("   ");
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public void ErrorOnNotification()
        {
            var logger1 = new Mock<ILogger<PushNotificationController>>();
            var notificator = new Mock<IPushNotificator>();
            notificator.Setup(x => x.PushNotification(It.IsAny<string>())).Throws(new Exception());
            
            var controller = new PushNotificationController(logger1.Object, notificator.Object);
            var result = controller.Post("test");
            Assert.IsInstanceOf<ObjectResult>(result);
            var temp = (ObjectResult) result;
            Assert.AreEqual(temp.StatusCode, 500);
        }
    }
}