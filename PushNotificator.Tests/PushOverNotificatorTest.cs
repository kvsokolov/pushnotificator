using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using PushNotificator.PushNotificator;

namespace PushNotificator.Tests
{
    public class PushOverNotificatorTest
    {
        [Test]
        public void WrongUrl()
        {
            var mock = new Mock<IConfiguration>();
            mock.SetupGet(p => p["PushOverSettings:AppToken"]).Returns("token");
            mock.SetupGet(p => p["PushOverSettings:UserKey"]).Returns("userKey");
            mock.SetupGet(p => p["PushOverSettings:RequestUrl"]).Returns("http://ya.ru");
            var notificator = new PushOverNotificator(mock.Object);
            Assert.Throws<WebException>(()=>notificator.PushNotification("test"));
        }

        [Test]
        public void WrongCredential()
        {
            var mock = new Mock<IConfiguration>();
            mock.SetupGet(p => p["PushOverSettings:AppToken"]).Returns("token");
            mock.SetupGet(p => p["PushOverSettings:UserKey"]).Returns("userKey");
            mock.SetupGet(p => p["PushOverSettings:RequestUrl"]).Returns("https://api.pushover.net/1/messages.json");
            var notificator = new PushOverNotificator(mock.Object);
            Assert.Throws<WebException>(() => notificator.PushNotification("test"));
        }
    }
}